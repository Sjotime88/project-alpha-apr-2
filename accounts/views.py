from django.contrib.auth import login
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect


# Create your views here.

def signup(request):

    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = request.POST["username"]
            password = request.POST["password"]
            user = User.objects.create_user(
                username=username,
                password=password,
            )
            user.save()  # user=form.save
            login(request, user)
            return redirect("home")

    else:
        form = UserCreationForm()

    return render(request, "registration/signup.html", {"form": form})

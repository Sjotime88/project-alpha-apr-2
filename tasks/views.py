from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView
from tasks.models import Task

# Create your views here.


class TaskCreateView(LoginRequiredMixin, CreateView):
    # Create a view that will show a form to create an instance of the Task model for all properties except the is_completed field
    model = Task
    template_name = "tasks/create.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]
    success_url = reverse_lazy("task_detail")

    def form_valid(self, form):
        item = form.save(commit=False)
        item.member = self.request.user
        item.save()
        return redirect(
            "projects/detail.html"
        )  # When the view successfully handles the form submission, have it redirect to the detail page of the task's project


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"

    def form_valid(self, form):
        form.instance.task.all = self.request.user
        return super().form_valid(form)

    # This feature allows a logged in person to see the tasks that are assigned to them.
    # Create a list view for the Task model with the objects filtered so that the person only sees the tasks assigned to them by filtering with the assignee equal to the currently logged in user#


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    # Create an update view for the Task model that only is concerned with the is_completed field
    fields = ["is_comlpeted"]
    success_url = reverse_lazy("show_my_tasks")
    # http://localhost:8000/tasks/mine/
